# VIA 2223

This is a webpage/blog of an app being made for the VIA subject

webpage description:

Visitors will be able to pan a map and they will get info about weather according to where on the map they're looking.
They will also be able to login to save and load favourite locations.

Combined APIs: a map API, a weather API, and a custom API for logins and favourite locations
